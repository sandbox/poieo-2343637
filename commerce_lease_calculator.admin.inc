<?php

/**
 * @file
 * Implements administrative functions for the Commerce Lease Calculator module.
 */

/**
 * Sets the lease calculator admin form.
 */
function commerce_lease_calculator_my_admin_form($form_state) {

  $index = 1;
  $form['buyout'] = array(
    '#type' => 'markup',
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#markup' => t('$1.00 Buy Out'),
  );
  $form['rates_dollar'] = array(
    '#tree' => TRUE,
    '#theme' => 'commerce_lease_calculator_textfield_table',
  );

  // Table for BuyOut = $.
  $rows = db_query("select * from {commerce_lease_calculator} WHERE buy_out = :buy_out", array(':buy_out' => "$"));
  foreach ($rows as $row) {
    $form['rates_dollar'][$index]['field0'] = array(
      '#title' => t('Low Value'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->low_lease,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_dollar'][$index]['field1'] = array(
      '#title' => t('High Value'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->high_lease,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_dollar'][$index]['field2'] = array(
      '#title' => t('24 Months'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->multiplier24,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_dollar'][$index]['field3'] = array(
      '#title' => t('36 Months'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->multiplier36,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_dollar'][$index]['field4'] = array(
      '#title' => t('48 Months'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->multiplier48,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_dollar'][$index]['field5'] = array(
      '#title' => t('60 Months'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->multiplier60,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_dollar'][$index]['field6'] = array(
      '#type' => 'textfield',
      '#default_value' => $row->id,
      '#size' => 9,
    );
    $index++;
  }

  // Table for 10% buyout.
  $form['buyout%'] = array(
    '#type' => 'markup',
    '#prefix' => '<p>',
    '#suffix' => '</p>',
    '#markup' => t('10% Buy Out'),
  );
  $form['rates_percent'] = array(
    '#tree' => TRUE,
    '#theme' => 'commerce_lease_calculator_textfield_table',
  );

  $rows = db_query("select * from {commerce_lease_calculator} WHERE buy_out = :buy_out", array(':buy_out' => "%"));
  foreach ($rows as $row) {
    $form['rates_percent'][$index]['field0'] = array(
      '#title' => t('Low Value'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->low_lease,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_percent'][$index]['field1'] = array(
      '#title' => t('High Value'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->high_lease,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_percent'][$index]['field2'] = array(
      '#title' => t('24 Months'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->multiplier24,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_percent'][$index]['field3'] = array(
      '#title' => t('36 Months'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->multiplier36,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_percent'][$index]['field4'] = array(
      '#title' => t('48 Months'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->multiplier48,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_percent'][$index]['field5'] = array(
      '#title' => t('60 Months'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $row->multiplier60,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $form['rates_percent'][$index]['field6'] = array(
      '#type' => 'textfield',
      '#default_value' => $row->id,
      '#size' => 9,
      '#element_validate' => array('element_validate_number'),
    );
    $index++;
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save new lease rates'),
    '#submit' => array('commerce_lease_calculator_form_submit'),
  );
  return $form;
}

/**
 * Clean the lease calculator row output.
 *
 * @param int $input
 *   The input value from the form submission.
 * @param string $filter
 *   The filter to apply to the $input value.
 *
 * @return int
 *   An associative array giving returned values filtered through check_plain.
 */
function _commerce_lease_calculator_clean_array($input, $filter = 'check_plain') {
  $clean = array();
  foreach ($input as $key => $val) {
    if (is_array($val)) {
      $clean[$filter($key)] = _commerce_lease_calculator_clean_array($val, $filter);
    }
    else {
      $clean[$filter($key)] = $filter($val);
    }
  }
  return $clean;
}

/**
 * Submit the lease calculator table.
 */
function commerce_lease_calculator_form_submit($form, $form_state) {
  // Save buyout = $.
  foreach ($form_state['values']['rates_dollar'] as $key) {
    $values = array(
      'low_lease' => $key['field0'],
      'high_lease' => $key['field1'],
      'multiplier24' => $key['field2'],
      'multiplier36' => $key['field3'],
      'multiplier48' => $key['field4'],
      'multiplier60' => $key['field5'],
      'id' => $key['field6'],
    );
    db_update('commerce_lease_calculator')->fields(array(
      'low_lease' => $values['low_lease'],
      'high_lease' => $values['high_lease'],
      'multiplier24' => $values['multiplier24'],
      'multiplier36' => $values['multiplier36'],
      'multiplier48' => $values['multiplier48'],
      'multiplier60' => $values['multiplier60'],
    ))->condition('id', $values['id'], '=')->execute();
  }

  // Save buyout = %.
  foreach ($form_state['values']['rates_percent'] as $key) {
    $values = array(
      'low_lease' => $key['field0'],
      'high_lease' => $key['field1'],
      'multiplier24' => $key['field2'],
      'multiplier36' => $key['field3'],
      'multiplier48' => $key['field4'],
      'multiplier60' => $key['field5'],
      'id' => $key['field6'],
    );
    db_update('commerce_lease_calculator')->fields(array(
      'low_lease' => $values['low_lease'],
      'high_lease' => $values['high_lease'],
      'multiplier24' => $values['multiplier24'],
      'multiplier36' => $values['multiplier36'],
      'multiplier48' => $values['multiplier48'],
      'multiplier60' => $values['multiplier60'],
    ))->condition('id', $values['id'], '=')->execute();
  }
  drupal_set_message(t('New lease rates have been saved'));
}
