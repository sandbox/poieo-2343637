CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Recommended profile
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
The Commerce Lease Calculator module uses a preset table of lease
variables to determine lease rates for the total price of commerce
products. A form is added to the add to cart form with options to select
the payoff type and length of term.

REQUIREMENTS
------------
This module requires the following modules:
* Drupal Commerce (https://www.drupal.org/project/commerce)

RECOMMENDED PROFILE
-------------------
* Commerce Kickstart (https://www.drupal.org/project/commerce_kickstart)

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
* Configure user permissions in Administration » People » Permissions:
   - Commerce Lease Calculator
     Users in roles with the "Access lease rate table" permission will be
     able to modify the lease rates.
* Set the lease rates in Store » Configuration » Lease Calculator Settings.
* To show the lease calculator on a product node, you must add a field to
   your product display content type with the machine name
   'commerce_lease_calculator'.  This should be a boolean, single on/off
   checkbox. **TODO** If this is going to be promoted to a full project,
   this field will need to be added by the module.
* Any product with the above field selected will have a lease calculator
   displayed when viewing.

MAINTAINERS
-----------
Current maintainers:
* Dave Heffron (Poieo) - https://drupal.org/user/303228
This project has been sponsored by:
* Poieo Design
   We are website design company specializing in Drupal systems for small 
   businesses, non-profits, and churches. We understand the needs of smaller
   organizations and want to help you create and grow your web presence.
   Visit http://www.poieodesign.com for more information.
